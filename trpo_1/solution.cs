﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trpo_1
{
    
    public class Solution
    {
        public History history = new History();
        public class Digits
        {
            List<char> digit = new List<char>() { '0', '1', '2','3','4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
            List<int> value = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

            public int returnValue(char digit) 
            {
                return this.value[this.digit.FindIndex(x => x == digit)];              
            }
            public char returnDigit(int value)
            {
                return this.digit[this.value.FindIndex(x => x == value)];
            }
        }
        Digits digits = new Digits();

        public static string ConvertToProb(string num, int sistema1, int sistema2)
        {
            int number = Convert.ToInt32(num, sistema1);
            string result = Convert.ToString(number, sistema2);
            return result;
        }
        public string ConvertToP2(double number, int sistema2)
        {
            string result, result1;

            int integer = Convert.ToInt32(Math.Truncate(number));
            double drob = number - integer;

            double ost;
            int help;
            help = Convert.ToInt32(Math.Truncate(number));
            List<char> perehod = new List<char>();
            do
            {
                ost = help % sistema2;
                
                help /= sistema2;
                perehod.Add(digits.returnDigit(Convert.ToInt32(ost)));
                if (help < sistema2 && help != 0)
                    perehod.Add(digits.returnDigit(Convert.ToInt32(help)));
            } while (help >= sistema2);
            perehod.Reverse();
            
            result = string.Join("", perehod.ToArray());
            if (drob == 0) return result;
            result += ',';

            perehod.Clear();
            double t;
            while (drob != 0) 
            {
                t = drob * sistema2;
                perehod.Add(digits.returnDigit(Convert.ToInt32(Math.Truncate(t))));
                drob *= sistema2;
                drob -= Convert.ToInt32(Math.Truncate(t));
            }
            result1 = string.Join("", perehod.ToArray());
            result += result1; 
            return result;
        }
        public  double ConvertFromP1(string num, int sistema1)
        {
            int digitsAfterPoint = 0;
            if (num.IndexOf(',') >= 0)
            {
                digitsAfterPoint = num.Length - num.IndexOf(',') - 1;
                num = num.Remove(num.IndexOf(','), 1);
            }
            double result = 0;

            int Length = num.Length;
            for (int i = 0; i < Length; i++)
                result += digits.returnValue(num[i]) * Math.Pow(sistema1, Length - 1 - i - digitsAfterPoint);
            return result;

            //int index = -1;
            //index = num.IndexOf(',');

            //if (index != -1)//если дробное
            //{
            //    double result = 0;
            //    string drob = num;
            //    drob = drob.Substring(index+1, num.Length-index-1);
            //    num = num.Substring(0, index);



            //    List<double> pere = new List<double>();


            //    double ost;
            //    if (sistema1 > 10)
            //        result = Convert.ToInt32(num, 16);
            //    else
            //    {
            //        double perehod = Convert.ToDouble(num);
            //        for (int i = 0; i < num.Length; i++)
            //        {
            //            ost = perehod % 10;
            //            perehod /= 10;
            //            pere.Add(Convert.ToInt32(ost));
            //        }

            //        for (int i = 0; i < pere.Count(); i++)
            //        {
            //            result += pere[i] * Math.Pow(sistema1, i);
            //        }
            //    }



            //    if (sistema1 > 10)
            //    {
            //        double perehod= Convert.ToInt32(drob, 16);
            //        pere.Clear();
            //        for (int i = 0; i < drob.Length; i++)
            //        {
            //            ost = perehod % 10;
            //            perehod /= 10;
            //            pere.Add(Convert.ToInt32(Math.Truncate(ost)));
            //        }
            //        pere.Reverse();
            //        for (int i = 0; i < pere.Count(); i++)
            //        {
            //            result += pere[i] * Math.Pow(sistema1, -i - 1);
            //        }
            //    }

            //    else 
            //    {
            //        pere.Clear();
            //        double perehod = Convert.ToDouble(drob);
            //        for (int i = 0; i < drob.Length; i++)
            //        {
            //            ost = perehod % 10;
            //            perehod /= 10;
            //            pere.Add(Convert.ToInt32(Math.Truncate(ost)));
            //        }
            //        pere.Reverse();
            //        for (int i = 0; i < pere.Count(); i++)
            //        {
            //            result += pere[i] * Math.Pow(sistema1, -i - 1);
            //        }
            //    }

            //    return result;
            //}

            //else
            //{
            //    ////if (sistema1 > 10)
            //    //    //return Convert.ToInt32(num, 16);
            //    //List<double> pere = new List<double>();
            //    ////int perehod = Convert.ToInt32(num, 16);
            //    //double perehod = Convert.ToDouble(num); 
            //    //double ost;
            //    //int t = perehod.ToString().Length;
            //    //for (int i = 0; i < t; i++) 
            //    //{
            //    //    ost = perehod % 10;
            //    //    perehod /= 10;
            //    //    pere.Add(Convert.ToInt32(Math.Truncate(ost)));
            //    //}
            //    //double result = 0;
            //    //for (int i = 0; i < pere.Count(); i++)
            //    //{
            //    //    result += pere[i] * Math.Pow(sistema1, i);

            //    //}
            //    return result;
            //}

        }
        public static void writehistory(string h1, string h2, string h3, string h4)
        {
            //hist.Text += Number_to_change.Text + " ";
            //hist.Text += P1.Value + "   ";
            //hist.Text += P2.Value + "   ";
            //hist.Text += result.Text + "\n";
        }
    }
}
