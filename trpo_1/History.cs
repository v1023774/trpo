﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trpo_1
{
    public class History
    {
        public int Length = 0;
        struct record
        {
            string number;
            decimal s1;
            decimal s2;
            string result;

            public record(string number, decimal s1, decimal s2, string result) {
                this.number = number;
                this.s1 = s1;
                this.s2 = s2;
                this.result = result;
            }

            public override string ToString() => $"{number}\t{s1}\t{s2}\t{result}";
        }

        List<record> Records = new List<record>();

        public void AddRecord(string number, decimal s1, decimal s2, string result)
        {
            Records.Add(new record(number, s1, s2, result));
            Length++;
        }
        public string returnRecord(int i)
        {
            return Records[i].ToString();
        }



        //public static void writeResult(string number, decimal s1, decimal s2, string result)
        //{
        //    Form1.hist.Text += Form1.Number_to_change.Text + " ";
        //    Form1.hist.Text += s1 + " ";
        //    Form1.hist.Text += s2 + " ";
        //    Form1.hist.Text += result + "\n";
        //}
    }
}

