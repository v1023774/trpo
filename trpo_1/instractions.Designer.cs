﻿
namespace trpo_1
{
    partial class instractions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inst = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // inst
            // 
            this.inst.Location = new System.Drawing.Point(12, 9);
            this.inst.Name = "inst";
            this.inst.Size = new System.Drawing.Size(213, 432);
            this.inst.TabIndex = 0;
            this.inst.Click += new System.EventHandler(this.inst_Click);
            // 
            // instractions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 450);
            this.Controls.Add(this.inst);
            this.Name = "instractions";
            this.Text = "instractions";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label inst;
    }
}