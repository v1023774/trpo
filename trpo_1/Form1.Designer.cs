﻿namespace trpo_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.P1 = new System.Windows.Forms.NumericUpDown();
            this.P2 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.n1 = new System.Windows.Forms.Button();
            this.n2 = new System.Windows.Forms.Button();
            this.n3 = new System.Windows.Forms.Button();
            this.n4 = new System.Windows.Forms.Button();
            this.n5 = new System.Windows.Forms.Button();
            this.n6 = new System.Windows.Forms.Button();
            this.n7 = new System.Windows.Forms.Button();
            this.n8 = new System.Windows.Forms.Button();
            this.n9 = new System.Windows.Forms.Button();
            this.nA = new System.Windows.Forms.Button();
            this.nB = new System.Windows.Forms.Button();
            this.nC = new System.Windows.Forms.Button();
            this.nD = new System.Windows.Forms.Button();
            this.nE = new System.Windows.Forms.Button();
            this.nF = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.n0 = new System.Windows.Forms.Button();
            this.twxtg = new System.Windows.Forms.TextBox();
            this.npoint = new System.Windows.Forms.Button();
            this.nCE = new System.Windows.Forms.Button();
            this.Number_to_change = new System.Windows.Forms.TextBox();
            this.hist = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.P1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.P2)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(5, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(56, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Справка";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(116, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "История";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(57, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "с. с. ч. ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // P1
            // 
            this.P1.Location = new System.Drawing.Point(5, 32);
            this.P1.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.P1.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(48, 20);
            this.P1.TabIndex = 4;
            this.P1.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.P1.ValueChanged += new System.EventHandler(this.P1_ValueChanged);
            // 
            // P2
            // 
            this.P2.Location = new System.Drawing.Point(5, 101);
            this.P2.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.P2.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(48, 20);
            this.P2.TabIndex = 6;
            this.P2.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "ваше число";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(57, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "желаемая с. с.";
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(5, 156);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(172, 20);
            this.result.TabIndex = 9;
            this.result.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 18);
            this.label4.TabIndex = 10;
            this.label4.Text = "Результат";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(5, 127);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(81, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Перевести";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // n1
            // 
            this.n1.Location = new System.Drawing.Point(51, 200);
            this.n1.Name = "n1";
            this.n1.Size = new System.Drawing.Size(38, 38);
            this.n1.TabIndex = 12;
            this.n1.Text = "1";
            this.n1.UseVisualStyleBackColor = true;
            this.n1.Click += new System.EventHandler(this.n1_Click);
            // 
            // n2
            // 
            this.n2.Enabled = false;
            this.n2.Location = new System.Drawing.Point(95, 200);
            this.n2.Name = "n2";
            this.n2.Size = new System.Drawing.Size(38, 38);
            this.n2.TabIndex = 13;
            this.n2.Text = "2";
            this.n2.UseVisualStyleBackColor = true;
            this.n2.Click += new System.EventHandler(this.n2_Click);
            // 
            // n3
            // 
            this.n3.Enabled = false;
            this.n3.Location = new System.Drawing.Point(139, 200);
            this.n3.Name = "n3";
            this.n3.Size = new System.Drawing.Size(38, 38);
            this.n3.TabIndex = 14;
            this.n3.Text = "3";
            this.n3.UseVisualStyleBackColor = true;
            this.n3.Click += new System.EventHandler(this.n3_Click);
            // 
            // n4
            // 
            this.n4.Enabled = false;
            this.n4.Location = new System.Drawing.Point(7, 244);
            this.n4.Name = "n4";
            this.n4.Size = new System.Drawing.Size(38, 38);
            this.n4.TabIndex = 15;
            this.n4.Text = "4";
            this.n4.UseVisualStyleBackColor = true;
            this.n4.Click += new System.EventHandler(this.n4_Click);
            // 
            // n5
            // 
            this.n5.Enabled = false;
            this.n5.Location = new System.Drawing.Point(51, 244);
            this.n5.Name = "n5";
            this.n5.Size = new System.Drawing.Size(38, 38);
            this.n5.TabIndex = 16;
            this.n5.Text = "5";
            this.n5.UseVisualStyleBackColor = true;
            this.n5.Click += new System.EventHandler(this.n5_Click);
            // 
            // n6
            // 
            this.n6.Enabled = false;
            this.n6.Location = new System.Drawing.Point(95, 244);
            this.n6.Name = "n6";
            this.n6.Size = new System.Drawing.Size(38, 38);
            this.n6.TabIndex = 17;
            this.n6.Text = "6";
            this.n6.UseVisualStyleBackColor = true;
            this.n6.Click += new System.EventHandler(this.n6_Click);
            // 
            // n7
            // 
            this.n7.Enabled = false;
            this.n7.Location = new System.Drawing.Point(139, 244);
            this.n7.Name = "n7";
            this.n7.Size = new System.Drawing.Size(38, 38);
            this.n7.TabIndex = 18;
            this.n7.Text = "7";
            this.n7.UseVisualStyleBackColor = true;
            this.n7.Click += new System.EventHandler(this.button10_Click);
            // 
            // n8
            // 
            this.n8.Enabled = false;
            this.n8.Location = new System.Drawing.Point(7, 288);
            this.n8.Name = "n8";
            this.n8.Size = new System.Drawing.Size(38, 38);
            this.n8.TabIndex = 19;
            this.n8.Text = "8";
            this.n8.UseVisualStyleBackColor = true;
            this.n8.Click += new System.EventHandler(this.n8_Click);
            // 
            // n9
            // 
            this.n9.Enabled = false;
            this.n9.Location = new System.Drawing.Point(51, 288);
            this.n9.Name = "n9";
            this.n9.Size = new System.Drawing.Size(37, 38);
            this.n9.TabIndex = 20;
            this.n9.Text = "9";
            this.n9.UseVisualStyleBackColor = true;
            this.n9.Click += new System.EventHandler(this.n9_Click);
            // 
            // nA
            // 
            this.nA.Enabled = false;
            this.nA.Location = new System.Drawing.Point(94, 288);
            this.nA.Name = "nA";
            this.nA.Size = new System.Drawing.Size(39, 38);
            this.nA.TabIndex = 21;
            this.nA.Text = "a";
            this.nA.UseVisualStyleBackColor = true;
            this.nA.Click += new System.EventHandler(this.nA_Click_1);
            // 
            // nB
            // 
            this.nB.Enabled = false;
            this.nB.Location = new System.Drawing.Point(139, 288);
            this.nB.Name = "nB";
            this.nB.Size = new System.Drawing.Size(38, 38);
            this.nB.TabIndex = 22;
            this.nB.Text = "b";
            this.nB.UseVisualStyleBackColor = true;
            this.nB.Click += new System.EventHandler(this.nB_Click);
            // 
            // nC
            // 
            this.nC.Enabled = false;
            this.nC.Location = new System.Drawing.Point(7, 332);
            this.nC.Name = "nC";
            this.nC.Size = new System.Drawing.Size(38, 38);
            this.nC.TabIndex = 23;
            this.nC.Text = "c";
            this.nC.UseVisualStyleBackColor = true;
            this.nC.Click += new System.EventHandler(this.nC_Click);
            // 
            // nD
            // 
            this.nD.Enabled = false;
            this.nD.Location = new System.Drawing.Point(51, 332);
            this.nD.Name = "nD";
            this.nD.Size = new System.Drawing.Size(38, 38);
            this.nD.TabIndex = 24;
            this.nD.Text = "d";
            this.nD.UseVisualStyleBackColor = true;
            this.nD.Click += new System.EventHandler(this.nD_Click);
            // 
            // nE
            // 
            this.nE.Enabled = false;
            this.nE.Location = new System.Drawing.Point(94, 332);
            this.nE.Name = "nE";
            this.nE.Size = new System.Drawing.Size(39, 38);
            this.nE.TabIndex = 25;
            this.nE.Text = "e";
            this.nE.UseVisualStyleBackColor = true;
            this.nE.Click += new System.EventHandler(this.nE_Click);
            // 
            // nF
            // 
            this.nF.Enabled = false;
            this.nF.Location = new System.Drawing.Point(139, 332);
            this.nF.Name = "nF";
            this.nF.Size = new System.Drawing.Size(38, 38);
            this.nF.TabIndex = 26;
            this.nF.Text = "f";
            this.nF.UseVisualStyleBackColor = true;
            this.nF.Click += new System.EventHandler(this.nF_Click);
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(7, 424);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(170, 38);
            this.delete.TabIndex = 28;
            this.delete.Text = "удалить";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // n0
            // 
            this.n0.Location = new System.Drawing.Point(7, 200);
            this.n0.Name = "n0";
            this.n0.Size = new System.Drawing.Size(38, 38);
            this.n0.TabIndex = 29;
            this.n0.Text = "0";
            this.n0.UseVisualStyleBackColor = true;
            this.n0.Click += new System.EventHandler(this.n0_Click_1);
            // 
            // twxtg
            // 
            this.twxtg.Location = new System.Drawing.Point(770, 31);
            this.twxtg.Name = "twxtg";
            this.twxtg.Size = new System.Drawing.Size(184, 20);
            this.twxtg.TabIndex = 30;
            // 
            // npoint
            // 
            this.npoint.Location = new System.Drawing.Point(7, 380);
            this.npoint.Name = "npoint";
            this.npoint.Size = new System.Drawing.Size(82, 38);
            this.npoint.TabIndex = 32;
            this.npoint.Text = ",";
            this.npoint.UseVisualStyleBackColor = true;
            this.npoint.Click += new System.EventHandler(this.npoint_Click);
            // 
            // nCE
            // 
            this.nCE.Location = new System.Drawing.Point(95, 380);
            this.nCE.Name = "nCE";
            this.nCE.Size = new System.Drawing.Size(82, 38);
            this.nCE.TabIndex = 33;
            this.nCE.Text = "CE";
            this.nCE.UseVisualStyleBackColor = true;
            this.nCE.Click += new System.EventHandler(this.nCE_Click);
            // 
            // Number_to_change
            // 
            this.Number_to_change.Location = new System.Drawing.Point(5, 58);
            this.Number_to_change.Name = "Number_to_change";
            this.Number_to_change.Size = new System.Drawing.Size(172, 20);
            this.Number_to_change.TabIndex = 9;
            this.Number_to_change.Text = "0";
            this.Number_to_change.TextChanged += new System.EventHandler(this.result_TextChanged);
            this.Number_to_change.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Number_to_change_KeyPress);
            // 
            // hist
            // 
            this.hist.Location = new System.Drawing.Point(5, 32);
            this.hist.Name = "hist";
            this.hist.Size = new System.Drawing.Size(172, 446);
            this.hist.TabIndex = 34;
            this.hist.Visible = false;
            this.hist.Click += new System.EventHandler(this.hist_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(189, 491);
            this.Controls.Add(this.hist);
            this.Controls.Add(this.nCE);
            this.Controls.Add(this.npoint);
            this.Controls.Add(this.twxtg);
            this.Controls.Add(this.n0);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.nF);
            this.Controls.Add(this.nE);
            this.Controls.Add(this.nD);
            this.Controls.Add(this.nC);
            this.Controls.Add(this.nB);
            this.Controls.Add(this.nA);
            this.Controls.Add(this.n9);
            this.Controls.Add(this.n8);
            this.Controls.Add(this.n7);
            this.Controls.Add(this.n6);
            this.Controls.Add(this.n5);
            this.Controls.Add(this.n4);
            this.Controls.Add(this.n3);
            this.Controls.Add(this.n2);
            this.Controls.Add(this.n1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Number_to_change);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.P2);
            this.Controls.Add(this.P1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Конвертор";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.P1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.P2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown P1;
        //public static System.Windows.Forms.TextBox Number_to_change;
        private System.Windows.Forms.NumericUpDown P2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button n1;
        private System.Windows.Forms.Button n2;
        private System.Windows.Forms.Button n4;
        private System.Windows.Forms.Button n5;
        private System.Windows.Forms.Button n6;
        private System.Windows.Forms.Button n7;
        private System.Windows.Forms.Button n8;
        private System.Windows.Forms.Button n9;
        private System.Windows.Forms.Button nA;
        private System.Windows.Forms.Button nB;
        private System.Windows.Forms.Button nC;
        private System.Windows.Forms.Button nD;
        private System.Windows.Forms.Button nE;
        private System.Windows.Forms.Button nF;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button n0;
        public System.Windows.Forms.TextBox twxtg;
        public System.Windows.Forms.Button n3;
        private System.Windows.Forms.Button npoint;
        private System.Windows.Forms.Button nCE;
        public System.Windows.Forms.TextBox Number_to_change;
        public System.Windows.Forms.Label hist;
    }
}

