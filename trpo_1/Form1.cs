﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using History;

namespace trpo_1
{
    public partial class Form1 : Form
    {
    
        public Form1()
        {
        
            InitializeComponent();
            
        }
        Solution solution = new Solution();


        private void button3_Click(object sender, EventArgs e)
        {
            if (hist.Visible)
                hist.Hide();
            else
                hist.Show();
        }
        public void enable(decimal Value, Button button, int s )
        {
            if (Value < s)
                button.Enabled = false;
            else
                button.Enabled = true;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
        private void P1_ValueChanged(object sender, EventArgs e)
        {
            enable(P1.Value, n2, 3);
            enable(P1.Value, n3, 4);
            enable(P1.Value, n4, 5);
            enable(P1.Value, n5, 6);
            enable(P1.Value, n6, 7);
            enable(P1.Value, n7, 8);
            enable(P1.Value, n8, 9);
            enable(P1.Value, n9, 10);
            enable(P1.Value, nA, 11);
            enable(P1.Value, nB, 12);
            enable(P1.Value, nC, 13);
            enable(P1.Value, nD, 14);
            enable(P1.Value, nE, 15);
            enable(P1.Value, nF, 16);          
        }
        private void button4_Click(object sender, EventArgs e)
        {
            double contvertation = solution.ConvertFromP1(Number_to_change.Text, (int)P1.Value);
            result.Text = solution.ConvertToP2(contvertation, (int)P2.Value);
            solution.history.AddRecord(Number_to_change.Text, (int)P1.Value, (int)P2.Value, result.Text);
            hist.Text += solution.history.returnRecord(solution.history.Length - 1) + "\n";
            
            //History.writeResult(Number_to_change.Text, (int)P1.Value, (int)P2.Value, result.Text) ;


        }

        private void n1_Click(object sender, EventArgs e)
        {
            
            Number_to_change.Text += n1.Text; 
        }

        public void n2_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n2.Text;
        }

        private void n3_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n3.Text;   
        }

        private void n4_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n4.Text;
        }

        private void n5_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n5.Text;
        }

        private void n6_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n6.Text;
        }
        private void button10_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n7.Text;
        }

        private void n8_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n8.Text;
        }

        private void n9_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n9.Text;
        }

        private void nA_Click_1(object sender, EventArgs e)
        {
            Number_to_change.Text += nA.Text;
        }

        private void nB_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += nB.Text;
        }

        private void nC_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += nC.Text;
        }

        private void nD_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += nD.Text;
        }

        private void nE_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += nE.Text;
        }

        private void nF_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += nF.Text;
        }

        private void n0_Click(object sender, EventArgs e)
        {
            Number_to_change.Text += n0.Text;
        }

        private void delete_Click(object sender, EventArgs e)
        {
            if (Number_to_change.Text.Length != 0)
                Number_to_change.Text = Number_to_change.Text.Substring(0, Number_to_change.Text.Length-1);
        }

        public void Number_to_change_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void result_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void n0_Click_1(object sender, EventArgs e)
        {
            Number_to_change.Text += n0.Text;
        }

        private void hist_Click(object sender, EventArgs e)
        {

        }

        private void Number_to_change_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            char number = e.KeyChar;
            if (number == 8 && Number_to_change.Text.Length == 1 || Number_to_change.Text == "")
            {
                Number_to_change.Text = "0";
                Number_to_change.SelectionStart = Number_to_change.Text.Length;
                e.Handled = true;
                return;
            }

            
            if ((e.KeyChar < 48 || e.KeyChar > 47+P1.Value) && number != 8 &&  number != ',') 

            {
                e.Handled = true;
            }
            
            if (( P1.Value >= 10 && e.KeyChar >= 97 && e.KeyChar <= 86 + P1.Value ))

            {              
                e.Handled = false;
                
            }
            if (Number_to_change.Text.First() == '0'
                && Number_to_change.Text.Length == 1 && number !=',' &&  number == '0')
            {
                e.Handled = true;
            }
            if (Number_to_change.Text.First() == '0' && Number_to_change.Text.Length == 1 && !e.Handled && number != ',')
            {
                e.Handled = true;
                Number_to_change.Text = number.ToString();
                Number_to_change.SelectionStart = Number_to_change.Text.Length;
                return;
            }

            if (number == ',' && !e.Handled)
            {
                if (Number_to_change.SelectionStart == 0 || Number_to_change.Text.Contains(','))
                {
                    e.Handled = true;
                    return;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            instractions instr = new instractions();
            instr.getinst();
            instr.Show();
        }

        private void npoint_Click(object sender, EventArgs e)
        { 
            if(!Number_to_change.Text.Contains(','))
                Number_to_change.Text += ','; 
        }

        private void nCE_Click(object sender, EventArgs e)
        {
            Number_to_change.Text = "0";
        }

        private void hist_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    
}
